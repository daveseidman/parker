// TODO: reexport all models with compression

import { WebGLRenderer, Scene, PerspectiveCamera,
  AmbientLight, DirectionalLight, BufferGeometry, BufferAttribute, Raycaster,
  CubeTextureLoader, Fog, FogExp2, Frustum,
  Mesh, Object3D, BoxGeometry, LinearFilter, MeshBasicMaterial, MeshNormalMaterial, MeshLambertMaterial, InstancedMesh,
  Vector2, Vector3, Color, Quaternion, Matrix4,
  Points, PointsMaterial } from 'three';
// import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { DepthOfFieldEffect, EffectComposer, EffectPass, RenderPass, BlendFunction } from 'postprocessing';
// import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
// import { BokehPass } from 'three/examples/jsm/postprocessing/BokehPass';
// import { EffectPass } from 'three/exmaples/jsm/postprocessing/EffectPass';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as dat from 'dat.gui';
import './index.scss';
import autoBind from 'auto-bind';
import Stats from 'stats-js';
import { Tween, Easing, update as updateTweens } from '@tweenjs/tween.js';

import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import px2 from './assets/images/gradient_1.png';
import nx2 from './assets/images/gradient_2.png';
import py2 from './assets/images/gradient_3.png';
import ny2 from './assets/images/gradient_4.png';
import pz2 from './assets/images/gradient_5.png';
import nz2 from './assets/images/gradient_6.png';


const draco = new DRACOLoader();
// draco.setDecoderPath('https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/js/libs/draco/'); // use a full url path
draco.setDecoderPath('./assets/draco/');

const degToRad = deg => deg * (Math.PI / 180);
const colors = [
  new Color('rgb(60, 100, 250)'),
  new Color('rgb(70, 120, 240)'),
  new Color('rgb(60, 151, 200)'),
];
// const randomColor = () => new Color(`rgb(${Math.floor(Math.random() * 100) + 0},${Math.floor(Math.random() * 100)},${Math.floor(Math.random() * 60) + 196})`);
const randomColor = () => colors[Math.floor(Math.random() * colors.length)];

const up = new Vector3(0, 1, 0);
const hidden = new Matrix4().makeScale(0, 0, 0);
const cameraDistanceThreshold = 20;
const cameraMoveThreshold = 10;
const cameraZoomThreshold = 10;
const zoomInDistance = 10;
const maxCars = 200000;
const maxCarsPerStreet = 100;
const carLengths = [0.6, 1, 1.4];

class App {
  constructor() {
    autoBind(this);

    this.config = {
      borough: 'test',
      cars: {
        variation: 1,
        spacing: 0.1,
        perpendictular: false,
      },
      streets: {
        width: 0.6,
        cornerSpace: 0.25,
        bothSides: true,
      },
    };

    // since we're not wrapping randomSequence, make sure it's long enough to
    // cover a street that starts near the end of the sequence and has a lot of cars
    // multiplying by 2 for two sided streets
    this.sequenceLength = maxCars + (maxCarsPerStreet * 2);
    this.randomSequence = [];

    for (let i = 0; i < this.sequenceLength; i += 1) {
      const random = Math.floor(Math.random() * 3);// this.carModels.length);
      this.randomSequence.push({
        type: random,
        carLength: carLengths[random],
        color: randomColor(),
      });
    }


    this.stats = new Stats();
    this.stats.showPanel(0);
    if (window.location.hostname === 'localhost') document.body.appendChild(this.stats.dom);

    this.setupScene().then(() => {
      this.setupConfig();
      this.changeMap();
      this.update();
    });

    this.results = { current: 0 };

    this.statsEl = document.createElement('div');
    this.statsEl.className = 'stats';

    const statsPopup = document.createElement('div');
    statsPopup.className = 'stats-popup';

    this.statsCarsParkedEl = document.createElement('h1');
    this.statsCarsParkedEl.innerText = '000000';

    const statsElLabel = document.createElement('p');
    statsElLabel.innerText = 'Cars Parked';
    statsPopup.appendChild(this.statsCarsParkedEl);
    statsPopup.appendChild(statsElLabel);

    this.statsEl.appendChild(statsPopup);

    document.body.appendChild(this.statsEl);

    window.addEventListener('resize', this.resize);
  }

  setupConfig() {
    const gui = new dat.GUI();
    gui.add(this.config, 'borough', this.boroughs.map(borough => borough.name)).onChange(this.changeMap);
    gui.add(this.config.cars, 'spacing', 0.01, 0.2).name('space between cars').onChange(this.placeCars);
    gui.add(this.config.cars, 'variation', 0.5, 2.00).name('car length variation').onChange(this.placeCars);
    gui.add(this.config.streets, 'cornerSpace', 0.2, 0.8).name('corner padding').onChange(this.placeCars);// val => this.update('street.cornerSpace', val));
    gui.add(this.config.streets, 'width', 0.4, 0.8).onChange(this.placeStreets);
    gui.add(this.config.cars, 'perpendictular').onChange(this.placeCars);
    gui.add(this.config.streets, 'bothSides').name('park on both sides').onChange(this.placeCars);
    gui.open();
  }

  setupScene() {
    this.scene = new Scene();
    // this.scene.fog = new FogExp2(0x7c91a3, 0.05);
    this.fog = new Fog(0x7c91a3, 0, 100);
    this.scene.fog = this.fog;
    this.camera = new PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 1000);
    this.camHeight = 120;
    this.camera.position.y = this.camHeight;
    this.renderer = new WebGLRenderer({ antialias: false,
      stencil: false, // TODO: see if this is needed
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.composer = new EffectComposer(this.renderer);
    this.composer.addPass(new RenderPass(this.scene, this.camera));

    // this.bokehPass = new BokehPass(this.scene, this.camera, {
    //   focus: 20,
    //   aperture: 0.0005,
    //   maxblur: 0.1,
    //   width: this.width / 2,
    //   height: this.height / 2,
    // });
    // this.composer.addPass(this.bokehPass);

    this.dofEffect = new DepthOfFieldEffect(this.camera, {
      /* blendFunction: BlendFunction.OVERLAY, */
      focusDistance: 0.013,
      focalLength: 0.02,
      bokehScale: 10,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2,
    });

    this.composer.addPass(new EffectPass(this.camera, this.dofEffect));
    document.body.appendChild(this.renderer.domElement);

    this.lastCameraPosition = this.camera.position.clone();
    this.lastCameraPositionCalc = this.camera.position.clone();
    this.sun = new DirectionalLight(0xffffff, 1);
    this.sun.target.position.set(-3, -10, 2);
    this.scene.add(this.sun);

    this.scene.background = new CubeTextureLoader().load([px2, nx2, py2, ny2, pz2, nz2]);

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.enableDamping = true;
    this.controls.maxPolarAngle = degToRad(75);

    // this.renderer.domElement.addEventListener('dblclick', this.zoom);
    // window.addEventListener('mousemove', )
    // this.sequenceLength = maxCars + (maxCarsPerStreet * 2);

    this.frustum = new Frustum();

    return new Promise((resolve) => {
      this.loadCar()
        .then(this.loadStreet)
        .then(this.loadCity)
        .then(resolve);
    });
  }

  zoom({ clientX, clientY }) {
    const pointer = new Vector2((clientX / window.innerWidth) * 2 - 1, -(clientY / window.innerHeight) * 2 + 1);
    const raycaster = new Raycaster();
    raycaster.setFromCamera(pointer, this.camera);
    const intersect = raycaster.intersectObject(this.scene.getObjectByName(`${this.config.borough}_land`));
    if (intersect[0]) {
      const { point } = intersect[0];
      const distance = this.camera.position.distanceTo(point);
      this[distance > cameraZoomThreshold ? 'zoomIn' : 'zoomOut'](point);
    }
  }

  zoomIn(point) {
    new Tween(this.controls).to({ maxDistance: zoomInDistance, target: point }).easing(Easing.Quadratic.InOut).start()
      .onComplete(() => { this.controls.maxDistance = this.maxDistance; });
  }

  zoomOut(point) {
    new Tween(this.controls).to({ minDistance: this.maxDistance }).easing(Easing.Quadratic.InOut).start()
      .onComplete(() => { this.controls.minDistance = 0; });
  }

  loadCar() {
    return new Promise((resolve) => {
      new GLTFLoader().setDRACOLoader(draco).load('assets/models/car2.gltf', (res) => {
        this.carModels = res.scene.children;
        this.carGeo = this.carModels[0].geometry;
        this.carLength = this.carModels[0].geometry.boundingBox.max.x - this.carModels[0].geometry.boundingBox.min.x;
        this.carWidth = this.carModels[0].geometry.boundingBox.max.z - this.carModels[0].geometry.boundingBox.min.z;
        this.carMat = new MeshLambertMaterial({ color: 0xffffff, reflectivity: 0.5 });
        return resolve();
      });
    });
  }

  loadStreet() {
    return new Promise((resolve) => {
      new GLTFLoader().setDRACOLoader(draco).load('assets/models/street-segment.gltf', ({ scene }) => {
        this.streetGeo = scene.getObjectByName('Street').geometry;
        this.cornerGeo = scene.getObjectByName('Corner').geometry;

        const map = scene.getObjectByName('Street').material.map;
        map.minFilter = LinearFilter;
        map.magFilter = LinearFilter;

        this.streetMat = new MeshLambertMaterial({ map });
        this.cornerMat = new MeshLambertMaterial({ map });

        return resolve();
      });
    });
  }

  loadCity() {
    return new Promise((resolve) => {
      new GLTFLoader().setDRACOLoader(draco).load('assets/models/streets2.gltf', ({ scene }) => {
        this.cityScene = scene;
        this.boroughs = scene.getObjectByName('boroughs').children;
        const landMap = new MeshBasicMaterial({ color: 0x3f723f });
        landMap.map = this.boroughs[0].children[2].material.map;
        this.boroughs.forEach((borough) => {
          const land = borough.children[2];
          land.visible = false;
          land.material = landMap;
          this.scene.add(borough.children[2]);
        });
        return resolve();
      });
    });
  }


  changeMap() {
    if (this.boroughPrev) this.scene.getObjectByName(`${this.boroughPrev}_land`).visible = false;
    this.streetsMesh = this.cityScene.getObjectByName(this.config.borough).children[0];
    this.scene.getObjectByName(`${this.config.borough}_land`).visible = true;
    const { min, max } = this.streetsMesh.geometry.boundingBox;
    const diagonal = min.distanceTo(max);
    this.camHeight = Math.atan(this.camera.aspect) * (diagonal);

    const landCenter = new Vector3((min.x + max.x) / 2, 0, (min.z + max.z) / 2);
    this.camera.position.copy(landCenter);
    this.camera.position.y = this.camHeight;
    this.controls.target.copy(landCenter);
    this.maxDistance = this.camera.position.distanceTo(this.controls.target);
    this.controls.maxDistance = this.maxDistance;

    const vertsArray = this.streetsMesh.geometry.attributes.position.array;
    const indexArray = this.streetsMesh.geometry.index.array;
    this.streets = [];


    // iterate through the geometry's index array which describes edges
    for (let i = 0; i < indexArray.length; i += 2) {
      const v1 = indexArray[i];
      const v2 = indexArray[i + 1];
      const start = new Vector3(vertsArray[(v1 * 3) + 0], vertsArray[(v1 * 3) + 1], vertsArray[(v1 * 3) + 2]);
      const end = new Vector3(vertsArray[(v2 * 3) + 0], vertsArray[(v2 * 3) + 1], vertsArray[(v2 * 3) + 2]);
      const angle = Math.atan2((start.x - end.x), (start.z - end.z)) + degToRad(90);
      const distance = start.distanceTo(end);
      const center = start.clone().add(end).divideScalar(2);
      const distanceToCamera = this.camera.position.distanceTo(center);
      const rotation = new Quaternion().setFromAxisAngle(up, angle);


      // build the streets array:
      this.streets.push({
        start,
        end,
        center,
        distance,
        distanceToCamera,
        angle,
        rotation,
        randomIndex: Math.floor(Math.random() * maxCars),
        showMeshes: false,
      });
    }


    // console.log(streetsCloseToCamera);

    if (this.instancedStreetMesh) this.scene.remove(this.instancedStreetMesh);
    if (this.instancedCornerMesh) this.scene.remove(this.instancedCornerMesh);
    // if (this.instancedCarMesh) this.scene.remove(this.instancedCarMesh);
    // if (this.pointsCarMesh) this.scene.remove(this.pointsCarMesh);
    this.instancedStreetMesh = new InstancedMesh(this.streetGeo, this.streetMat, this.streets.length);
    this.instancedCornerMesh = new InstancedMesh(this.cornerGeo, this.cornerMat, this.streets.length);
    // this.instancedCarMesh = new InstancedMesh(this.carGeo, this.carMat, streetsCloseToCamera * (this.maxCarsPerStreet * 2));
    this.scene.add(this.instancedStreetMesh);
    this.scene.add(this.instancedCornerMesh);
    // this.scene.add(this.instancedCarMesh);

    if (this.points) this.scene.remove(this.points);
    this.pointsGeo = new BufferGeometry();
    this.positions = new Float32Array(this.streets.length * maxCarsPerStreet * 2 * 3);
    this.pointsGeo.setAttribute('position', new BufferAttribute(this.positions, 3));
    this.points = new Points(this.pointsGeo, new PointsMaterial({ size: 0.33 }));

    this.scene.add(this.points);

    this.streetsLengthPrev = this.streets.length;
    this.boroughPrev = this.config.borough;

    this.cameraMoved();

    this.placeStreets();
  }

  placeStreets() {
    for (let i = 0; i < this.streets.length; i += 1) {
      const { start, end, rotation, distance } = this.streets[i];
      const halfWidthMatrix = new Matrix4().compose(start, rotation, new Vector3(this.config.streets.width / 1, 1, this.config.streets.width / 1));

      const streetMatrix = new Matrix4();
      const scale = new Vector3((distance / 2) - (this.config.streets.width), 1, this.config.streets.width);

      streetMatrix.compose(start.clone().add(end).divideScalar(2), rotation, scale);
      this.instancedStreetMesh.setMatrixAt(i, streetMatrix);
      this.instancedCornerMesh.setMatrixAt(i, halfWidthMatrix);
    }

    for (let i = this.streets.length; i < this.streetsLengthPrev; i += 1) {
      this.instancedStreetMesh.setMatrixAt(i, hidden);
      this.instancedCornerMesh.setMatrixAt(i, hidden);
    }

    this.instancedStreetMesh.instanceMatrix.needsUpdate = true;
    this.instancedCornerMesh.instanceMatrix.needsUpdate = true;

    this.placeCars();
  }

  placeCars() {
    this.carsParked = 0;
    this.carMeshes = 0;

    for (let i = 0; i < this.instancedCarMesh.count; i += 1) {
      this.instancedCarMesh.setMatrixAt(i, hidden);
    }

    for (let i = 0; i < this.positions.length; i += 1) {
      this.positions[i] = 0;
    }

    const { width } = this.config.streets;
    // const positionArray = [];
    this.streets.forEach(({ start, end, randomIndex, showMeshes }) => {
      for (let side = 0; side < (this.config.streets.bothSides ? 2 : 1); side += 1) {
        const forward = Math.atan2((start.x - end.x), (start.z - end.z));
        const backward = forward + degToRad(180);
        const right = forward + degToRad(90);
        const left = forward - degToRad(90);
        const streetLength = start.distanceTo(end);
        const streetPosition = new Vector3(start.x, start.y, start.z);
        let index = randomIndex + (side ? maxCarsPerStreet : 0);
        let distFromStart = this.config.streets.cornerSpace + (this.config.streets.width / 2);

        // keep placing cars down the street until we hit the corner
        while (distFromStart < streetLength - this.config.streets.cornerSpace - (this.config.streets.width / 2)) {
          const { carLength, color } = this.randomSequence[index]; // this.randomSequence[index + i];
          const carPosition = streetPosition.clone();
          // push car down the block
          carPosition.add(new Vector3(Math.sin(forward) * -distFromStart, 0, Math.cos(forward) * -distFromStart));

          // push car to side of street
          carPosition.add(new Vector3(Math.sin(side ? right : left) * (width - (this.carWidth * 2)), 0, Math.cos(side ? right : left) * (width - (this.carWidth * 2))));

          if (showMeshes) {
            this.carMeshes += 1;
            const carRotation = new Quaternion().setFromAxisAngle(up, this.config.cars.perpendictular ? (side ? backward : forward) : right);
            const carScale = new Vector3(carLength * this.config.cars.variation, 1, 1);
            const carMatrix = new Matrix4().compose(carPosition, carRotation, carScale);
            this.instancedCarMesh.setMatrixAt(this.carsParked, carMatrix);
            this.instancedCarMesh.setColorAt(this.carsParked, color);
          } else {
            this.positions[(this.carsParked * 3) + 0] = carPosition.x;
            this.positions[(this.carsParked * 3) + 2] = carPosition.z;
          }
          this.positions[(this.carsParked * 3) + 0] = carPosition.x;
          this.positions[(this.carsParked * 3) + 2] = carPosition.z;
          index += 1;
          this.carsParked += 1;
          distFromStart += (this.config.cars.perpendictular ? this.carWidth : (carLength * this.config.cars.variation) * this.carLength) + this.config.cars.spacing;
        }
      }
    });

    // TODO: look into this again, should be more efficient than resetting all beforehand
    // for (let i = this.carsParkedPrev; i < this.carsParked; i += 1) {
    //   this.positions[(i * 3) + 0] = 0;
    //   this.positions[(i * 3) + 2] = 0;
    // }

    this.points.geometry.setDrawRange(0, this.carsParkedPrev * 3);
    this.points.geometry.attributes.position.needsUpdate = true;


    if (this.carMeshes) {
      this.instancedCarMesh.instanceMatrix.needsUpdate = true;
      this.instancedCarMesh.instanceColor.needsUpdate = true;
    }
    if (this.updateStatsTimeout) clearTimeout(this.updateStatsTimeout);
    if (this.statsPrev !== this.carsParked) this.updateStatsTimeout = setTimeout(this.updateStats, 500);

    this.carsParkedPrev = this.carsParked;
    this.carsMeshesPrev = this.carsMeshes;

    // this.resultsAmount.innerText = this.carsParked.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    this.render();
  }

  update() {
    this.stats.begin();
    if (!this.cameraTween) this.controls.update();
    updateTweens();
    this.fog.far = this.camera.position.y * 10;
    this.dofEffect.bokehScale = 10 / this.camera.position.y;
    // this.dofEffect.target = this.controls.target;
    // this.dofEffect.calculateFocusDistance(this.controls.target);
    // this.bokehPass.uniforms.focus = this.camera.position.distanceTo(this.controls.target) / 2;
    if (this.camera.position.distanceTo(this.lastCameraPosition) > 0.01) this.render();
    if (this.camera.position.distanceTo(this.lastCameraPositionCalc) > cameraMoveThreshold) this.cameraMoved();
    this.lastCameraPosition = this.camera.position.clone();
    this.stats.end();
    requestAnimationFrame(this.update);
  }

  render() {
    // this.renderer.render(this.scene, this.camera);
    this.composer.render();
  }

  cameraMoved() {
    this.camera.updateMatrix(); // make sure camera's local matrix is updated
    this.camera.updateMatrixWorld(); // make sure camera's world matrix is updated
    this.camera.matrixWorldInverse.getInverse(this.camera.matrixWorld);
    this.frustum.setFromMatrix(new Matrix4().multiplyMatrices(this.camera.projectionMatrix, this.camera.matrixWorldInverse));

    this.lastCameraPositionCalc = this.camera.position.clone();
    let streetsWithMeshes = 0;
    this.streets.forEach((street) => {
      // TODO: will need to be a blend of cam and target position
      const distanceToCamera = street.center.distanceTo(this.camera.position);
      const inView = this.frustum.containsPoint(street.start) || this.frustum.containsPoint(street.end);

      street.showMeshes = distanceToCamera < cameraDistanceThreshold && inView;
      streetsWithMeshes += distanceToCamera < cameraDistanceThreshold;
    });

    if (this.instancedCarMesh) this.scene.remove(this.instancedCarMesh);
    this.instancedCarMesh = new InstancedMesh(this.carGeo, this.carMat, streetsWithMeshes * (maxCarsPerStreet * 2));
    this.scene.add(this.instancedCarMesh);
    this.placeCars();
  }

  updateStats() {
    this.statsPrev = this.carsParked;
    this.statsEl.classList.add('active');
    setTimeout(() => { this.statsEl.classList.remove('active'); }, 1500);
    this.statsTween = new Tween(this.results).to({ current: this.carsParked }, 1000).easing(Easing.Linear.None).onUpdate(({ current }) => {
      this.statsCarsParkedEl.innerText = Math.floor(current).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    })
      .start();
  }

  resize() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.render();
  }
}

const app = new App();
if (window.location.hostname === 'localhost') window.app = app;
