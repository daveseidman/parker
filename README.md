# Parker

WebGL visualization of parking methods for NYC streets. Exploration of different "packing methods" Secondary motive is to determine if there would me much improvement in the amount of available parking if everyone parked perpendicular to the curb on one side of the street only.

## TODO
- Add SAO pass
- Double click to zoom in / out
- Clean up street meshes in blender (remove stray verts)
- Adjust scale to match real world
- Fix Car length variation
